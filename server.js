const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config({ path: './.env'})

const app = require('./app');

const port = 3001;

console.log(process.env.NODE_ENV)

if(process.env.NODE_ENV === 'production'){
  mongoose.connect(`mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@cluster0-tdenb.mongodb.net/starwars?retryWrites=true&w=majority`, {useNewUrlParser: true, useFindAndModify: false})
  .then(() => console.log("Connected to db"))
  .catch(err => console.log(err))
} else {
  mongoose.connect('mongodb://localhost:27017/starwars', {useNewUrlParser: true, useFindAndModify: false})
    .then(() => console.log("Connected to db"))
}


const server = app.listen(port, () => {
  console.log(`App running on port ${port} ...`);
});