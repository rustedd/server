const Segment = require('../models/segmentModel');
const catchAsync = require("../utils/catchAsync");
const AppError = require('../utils/appError');
const Chapter = require('../models/chapterModel');
const Particle = require('../models/particleModel');

exports.getSegments = catchAsync(async (req, res, next) => {
  if(!req.query.appId){
    res.status(400).send('missing appId in query')
  }
  let segments = await Segment.find({ app: req.query.appId, isDeleted: { "$ne": true } });
  res.status(200).json({ segments })
});

/**
 * @api {get} /segment Get a list of all apps
 * @apiName CreateSegment
 * @apiGroup Segment
 * 
 * @apiParam {String} title Title of the Segment
 * @apiParam {String} description Description of the Segment
 * @apiParam {String} app appId of the app to which segment is added 
 * @apiParam {Number} rank Rank of the segment
 */

exports.createSegment = catchAsync(async (req, res, next) => {
  let newSegment = await Segment.create({
    ...req.body,
    app: req.body.appId
  })

  res.status(200).json({
    segment: newSegment,
    status: 'success'
  })
});

exports.patchSegment = catchAsync(async (req, res, next) => {
  let segment = await Segment.findOneAndUpdate(
    { _id: req.params.id }, 
    req.body, 
    { new: true }
  );

  if(!segment){
    return res.status(500).send({ error: "segment does not exist"});
  }

  res.send({ segment })
});

exports.deleteSegment = catchAsync(async (req, res, next) => {
  let segment = await Segment.findByIdAndUpdate(req.params.id, { isDeleted: true });
  await Chapter.updateMany({ segment }, { isDeleted: true });
  let chapters = await Chapter.find({ segment });
  for(let i = 0; i < chapters.length; i++){
    await Particle.updateMany({ chapter: chapters[i] }, { isDeleted: true })
  }
  res.status(204).json({
    status: 'success',
    data: chapters
  });
})