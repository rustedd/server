const Session = require('../models/sessionModel');
const UserStatus = require('../models/userStatusModel');

const catchAsync = require("../utils/catchAsync");
const AppError = require('../utils/appError')

exports.getSessions = catchAsync(async (req, res, next) => {
  let sessions = await Session.find({
    user: req.body.user
  })

  res.send(sessions);
})

exports.postSessions = catchAsync(async (req, res, next) => {
  if(!req.body.app  ){
    return next(new AppError(`Missing fields appId or chapterId `, 404))
  };
  
  let { sessionToSync } = req.body;
  let arrayOfSessions = [];
  Object.keys(sessionToSync).forEach((key, index) => {
    arrayOfSessions.push({
      user: req.user,
      app: req.body.app,
      chapter: sessionToSync[key].chapterId,
      status: sessionToSync[key].status,
      ...sessionToSync[key],
      ...sessionToSync[key].sessionDetails
    })
  })

  // adding sesion to user status 
  // DELETE USERSTATUS MODEL ITSELF> IT IS NOT NEEDED

  // let newUserStatus = await UserStatus.findOneAndUpdate({
  //   user: req.body.user,
  //   app: req.body.app
  // }, {
  //   $addToSet: {
  //     [`${req.body.status.toLowerCase()}Chapters`]: req.body.chapter
  //   }
  // },
  // {upsert: true, new: true})


  let session = await Session.insertMany(arrayOfSessions);
  
  res.status(200).json({
    status: 'success'
  })
})