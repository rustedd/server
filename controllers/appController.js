const App = require('../models/appModel');
const Segment = require('../models/segmentModel');
const Chapter = require('../models/chapterModel');
const Session = require('../models/sessionModel');
const catchAsync = require("../utils/catchAsync");

const contentGenerator = require('./autoGenerateApp')

const AppError = require('../utils/appError')

/**
 * @api {get} /app Get a list of all apps
 * @apiName GetUser
 * @apiGroup App
 */

exports.getApps = catchAsync(async (req, res, next) => {
  let apps = await App.find({ owner: req.user._id})
  res.status(200).json({ apps })
});

/**
 * @api {post} /app Create New App
 * @apiName PostUser
 * @apiGroup App
 * 
 * @apiParam {String} title Title of the App
 * @apiParam {String} description Description of the App
 */

exports.createApp = catchAsync(async (req, res, next) => {
  let newApp = await App.create({
    ...req.body,
    owner: req.user._id
  });


  await contentGenerator.createAppSample(newApp._id)

  res.status(200).json({
    app: newApp,
    status: 'success'
  })
});

exports.patchParticle = catchAsync(async (req, res, next) => {
  let app = await App.findOneAndUpdate(
    { _id: req.params.id }, 
    req.body, 
    { new: true }
  );

  if(!app){
    return res.status(500).send();
  }

  res.send({ app })

})

/**
 * @api {get} /app/:id Get App details
 * @apiName Get App Details
 * @apiGroup App With Status
 * 
 * @apiParam {Number} id App unique ID.
 */

exports.getAppDetails = catchAsync(async (req, res, next) => {
  let lastUpdated = 0;
  if(req.query.lastUpdated){
    lastUpdated = parseInt(req.query.lastUpdated);
  }
  
  let app = await App.findById(req.params.id);
  let query = { app: app, updatedAt:{"$gte": new Date(lastUpdated)}, isDeleted: false }

  let segments = await Segment.find(query).sort({rank: 1});
  let chapters = await Chapter.find(query).sort({rank: 1})
  if (!app) {
    return next(new AppError(`No app found with that id`, 404))
  }
  res.status(200).json({ app, segments, chapters })
});

/**
 * @api {get} /app/with-status/:id Get App details with status of user
 * @apiName Get App Details
 * @apiGroup App With Status
 * 
 * @apiParam {Number} id App unique ID.
 */


exports.getAppDetailsWithStatus = catchAsync(async (req, res, next) => {
  let lastUpdated = 0;
  if(req.query.lastUpdated){
    lastUpdated = parseInt(req.query.lastUpdated);
  }
  let sessions = await Session.find({ user: req.user });
  let app = await App.findById(req.params.id);

  let query = { 
    app: app, 
    updatedAt:{"$gte": new Date(lastUpdated)},
    isDeleted: { "$ne": true },
    isPublished: true
  };

  if(lastUpdated != 0) {
    query = {
      app: app, 
      updatedAt:{"$gte": new Date(lastUpdated)},
    }
  }

  if(req.query.type && req.query.type === "GAME"){
    query.type = {"$eq": "GAME"}
  } else {
    query.type = {"$ne": "GAME"}
  }


  let segments = await Segment.find(query).sort({rank: 1});
  let chapters = await Chapter.find(query).sort({rank: 1});


  chapters.forEach(chapter => {
    let chapterStatus;
    let chapterSessions = sessions.reduce((accumulator, session) => {
      if(chapter._id.equals(session.chapter)){
        accumulator.push(session);
      }
      return accumulator;
    }, []);
    if(chapterSessions){
      chapterSessions.forEach(session => {
        if(chapterStatus !== "COMPLETE"){
          chapterStatus = session.status;
        }
      })
    }
    chapter.status = chapterStatus;
  })

  if (!app) {
    return next(new AppError(`No app found with that id`, 404))
  }
  res.status(200).json({ app, segments, chapters })
});

// TODO: Deprecated, delete after testing
// exports.updateChapterList = catchAsync(async (req, res, next) => {
//   let app = await App.findById(req.params.id).populate('chapters');
//   let lastUpdated = 0;
//   if(req.query.lastUpdated){
//     lastUpdated = req.query.lastUpdated;
//   };

//   let chapters = [];
//   app.chapters.forEach(chapter => {
//     let chapterLastUpdated = new Date(chapter.updatedAt).getTime();
//     if(chapter.isPublished && chapterLastUpdated > lastUpdated){
//       chapters.push(chapter)
//     }
//   })

//   res.send({ chapters: chapters})
// })

exports.deleteApp = catchAsync(async (req, res, next) => {
  
});