const Segment = require('../models/segmentModel');
const Chapter = require('../models/chapterModel');
const Particle = require('../models/particleModel');

exports.createAppSample = async (appId) => {
  let firstSegment = await Segment.create({
    title: "Segment one",
    description: "This is a sample segment",
    rank: 100,
    app: appId
  })

  let firstChapter = await Chapter.create({
    title: "Chapter one",
    description: "This is a sample chapter",
    rank: 100,
    app: appId,
    segment: firstSegment._id
  })

  console.log(firstChapter)

  let sampleNote = await Particle.create({
    type: "NOTE",
    rank: 100,
    chapter: firstChapter._id,
    title: "This is a sample note",
    text: "this is dummy content for the chapter"
  })

  console.log(sampleNote)

  let sampleMCQ = await Particle.create({
    type: "MCQ",
    rank: 200,
    chapter: firstChapter._id,
    question: "This is a dummy question for reference?",
    options: [{
      text: "this is not an answer"
    }, {
      text: "this also is not an answer"
    }, {
      text: "this is an answer",
      isAnswer: true
    }, {
      text: "there can be only one answer"
    }]
  })

  return;
}