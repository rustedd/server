const Student = require('../models/studentModel');
const catchAsync = require("../utils/catchAsync");
const AppError = require('../utils/appError')
const mongoose = require('mongoose')

exports.getStudent = catchAsync(async (req, res, next) => {
  const student = await Student.find({
    studentEmail: req.query.email
  })

  res.status(200).json({
    student
  })
})

exports.getAllStudents = catchAsync(async (req, res, next) => {
  const students = await Student.find({
    appId: mongoose.Types.ObjectId(req.query.appId)
  }).sort('studentEmail')

  res.status(200).json({
    students
  })
});

exports.deleteStudent = catchAsync(async (req, res, next) => {
  const student = await Student.findByIdAndDelete(req.query.id);

  if(!student){
    return res.status(404).json({
      message: 'Student not found'
    })
  }

  res.status(204).json({
    status: 'success',
    data: null
  });
})

exports.postStudent = catchAsync(async (req, res, next) => {
  if(!req.body.appId) {
    return next(new AppError('Invalid app id', 400));
  }
  if(!req.body.studentEmail) {
    return next(new AppError('Student email missing', 400));
  }

  let newStudent = await Student.create({
    appId: req.body.appId,
    studentEmail: req.body.studentEmail
  })

  res.status(200).json({
    student: newStudent
  })
})