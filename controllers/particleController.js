const axios = require('axios')
const mongoose = require('mongoose')

const Particle = require('../models/particleModel');
const Chapter = require('../models/chapterModel');

const catchAsync = require("../utils/catchAsync");
const factory = require("./handlerFactory")

exports.addParticle = catchAsync(async (req, res, next) => {
  //TODO: add transactions to make this correct
  if(!req.body.chapterId) return res.status(500).send({ error: "chapterId is missing"});
  if(!req.body.rank) return res.status(500).send({ error: "rank is missing" });
  let particle = await Particle.create({ ...req.body, chapter: req.body.chapterId});
  let chapter = await Chapter.findOneAndUpdate(
    { _id: req.body.chapterId}, 
    {$push: {particles: particle._id}}, 
    {new: true}
  );

  if(!chapter){
    return res.status(500).send();
  }  
  res.send({chapter, particle})
})

exports.patchParticle = catchAsync(async (req, res, next) => {

  // if(!req.body.particleId) return res.status(500).send();
  if(!req.body.hint){
    // delete req.body.hint; // this is because passing empty string is throwing errors
    req.body.hint = undefined;
  }

  let particle = await Particle.findOneAndUpdate(
    { _id: req.params.id }, 
    req.body, 
    { new: true }
  );
  if(!particle){
    return res.status(500).send({ message: "particle not found"});
  }

  res.send({ particle })
})

exports.deleteParticle = catchAsync(async (req, res, next) => {
  // TODO: make this a pre hook
  // const chapter = await Chapter.findOneAndUpdate({}, {$pull: {particles: req.params.id}}, { new: true });
  const doc = await Particle.findByIdAndUpdate(req.params.id, { isDeleted: true });

  if (!doc) {
    return res.status(404).send("document not found");
  }


  res.status(204).json({
    status: 'success',
    data: null
  });
});

function getIdFromUrl(url) { 
  let regexMatch = url.match(/[-\w]{25,}/);
  if(regexMatch){
    return regexMatch[0]
  } else {
    return null;
  }
}

const runIfNoError = (res, error, message) => {
  if(!error){
    return res.status(500).send({message});
  } else {
    return null;
  }
}

exports.importFromGoogle = catchAsync(async (req, res, next) => {
  /*
   * First we check if chapter has particles and if yes we need to get its highest rank
   * 

  */
  if(!req.body.url) return res.status(500).send({ message: "URL is missing"})
  if(!req.body.chapterId) return res.status(500).send({ error: "chapterId is missing"});

  let documentId = getIdFromUrl(req.body.url);
  if(!documentId){
    return res.status(500).send({ message: "Invalid URL"})
  }

  let googleSheetData = null;

  try {
    let url = `https://sheets.googleapis.com/v4/spreadsheets/${documentId}/values/Sheet1?key=${process.env.GOOGLE_API_KEY}`
    googleSheetData = await axios.get(url)
    googleSheetData = googleSheetData.data;
  } catch (err) {
    return res.status(500).send({ message: "Error fetching data from google sheet. Ensure the sheet is public.", err: err})
  }


  let chapter = await Chapter.findById(req.body.chapterId)

  if(!chapter){
    return res.status(500).send("The chapter does not exist")
  }

  let biggestParticle = await Particle.find({ chapter: mongoose.Types.ObjectId(req.body.chapterId) }).sort({ rank: 1 }).limit(1);
  biggestParticle = biggestParticle[0];

  let highestRank = 0;
  if(biggestParticle){
    highestRank = biggestParticle.rank
  }

  let arrayOfParticles = []

  let error = false;

  googleSheetData.values.forEach((value, index) => {
    if(error) return;
    let particle = {};
    if(index === 0) {
      return false;
    } else {
      if(value[0] === "NOTE"){
        particle.type = "NOTE"
        if(value[1]){
          particle.title = value[1]
        } else {
          runIfNoError(res, error, `Row ${index + 1} is missing title`)
          error = true;
          return
        }
        if(value[2]){
          particle.text = value[2]
        } else {
          runIfNoError(res, error, `Row ${index + 1} is missing body`)
          error = true;
          return
        }
          
      } else if (value[0] === "MCQ"){
        particle.type = "MCQ"
        // ADDING QUESTION

        let answerIndex = parseInt(value[7]);
        if(isNaN(answerIndex)){
          answerIndex = undefined;
        }
        if(value[1]){
          particle.question = value[1]
        } else {
          runIfNoError(res, error, `Row ${index + 1} is missing question`)
          error = true;
          return
        }
        // ADDING OPTIONS
        if(!value[2] && !value[3] && !value[4] && !value[5]){
          runIfNoError(res, error, `Row ${index + 1} is missing options`)
          error = true;
          return
        } else {
          value.slice(2, 6).forEach((item, index) => {
            if(item){
              let option = {
                text: item
              }

              if(answerIndex){
                if(index === answerIndex - 1){
                  option.isAnswer = true;
                }
              }

              if(particle.options){
                particle.options.push(option);
              } else {
                particle.options = [option]
              }
            }            
          })
          if(!particle.options){
            runIfNoError(res, error, `Row ${index + 1} is missing options`)
            error = true;
            return
          }
        }

        if(value[7]){
          particle.tip = value[8]
        }

      } else {
        runIfNoError(res, error, `Row ${index + 1} is missing or has an invalid type`)
        error = true;
        return
      }
    }

    particle.chapter = req.body.chapterId
    highestRank += 100;
    particle.rank = highestRank;
    arrayOfParticles.push(particle)
  })

  
  if(!error) {
    let insertParticle = await Particle.insertMany(arrayOfParticles)
    res.status(200).json({
      status: 'success',
      data: null,
      id: documentId,
      data: insertParticle
    })
  }

})