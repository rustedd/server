const { promisify } = require('util');
const jwt = require('jsonwebtoken')
const AppUser = require('../models/appUserModel');

const AppError = require('../utils/appError')
const catchAsync = require("../utils/catchAsync");

const signToken = id => {
  return jwt.sign({ id: id }, process.env.JWT_SECRET, {
    expiresIn: '999y'
  }); 
};

exports.signup = catchAsync(async (req, res, next) => {
  // TODO : OPTIMISE THIS FOR SECURITY AND STUFF> THIS IS STUPID
  // const newUser = await AppUser.create({
  //   name: req.body.name,
  //   email: req.body.email,
  //   password: req.body.password,
  //   passwordConfirm: req.body.passwordConfirm
  // });

  if(req.body.email && (!req.body.password || !req.body.passwordConfirm)){
    return next(new AppError('Please provide password and passwordConfirm', 400))
  }

  const newUser = await AppUser.create({...req.body})
  const token = signToken(newUser._id)

  res.status(201).json({
    status: 'success',
    token: token,
    email: newUser.email,
    data: {
      user: newUser
    }
  })
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password, appId } = req.body;

  if(!email || !password){
    return next(new AppError('Please provide email and password ', 400));
  }

  const user = await AppUser.findOne({ email, appId }).select('+password');

  if(!user || !await user.correctPassword(password, user.password)){
    return next(new AppError('Incorrect email or password ', 401))
  }

  const token = signToken(user._id);
  res.status(200).json({
    status: 'success',
    token,
    email,
    id: user._id
  })
});

exports.updateUser = catchAsync(async (req, res, next) => {
  console.log(req.user);
  const user = await AppUser.findById(req.user._id);

  let { email, password, passwordConfirm, username } = req.body;


  if(email){
    user.email = email;
  }

  if(password){
    user.password = password;
    user.passwordConfirm = passwordConfirm;
  }
  
  if(username){
    user.username = username;
  }

  await user.save();

  const token = signToken(user._id);
  res.status(200).json({
    status: 'success',
    token,
    email: user.email,
    id: user._id
  })
})

exports.protect = catchAsync(async (req, res, next) => {
  let token;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  }

  if(!token) {
    return next(new AppError('You are not logged in ', 401));
  }
  
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)
  
  const freshUser = await AppUser.findById(decoded.id);
  if(!freshUser) {
    return next(new AppError('the user belonging to the id no longer exists', 401));
  }
  
  if(freshUser.changedPasswordAfter(decoded.iat)){
    return next(new AppError('User changed password log in again', 401))
  }

  req.user = freshUser; 

  next()
});

exports.updateToken = catchAsync(async (req, res, next) => {
  let { onesignalId } = req.body;

  const user = await AppUser.findById(req.user._id);
  console.log(user)
  if(!onesignalId){
    return res.status(500).json({
      status: 'failure',
      message: 'add onesignalId'
    })
  }
  user.onesignalId = onesignalId;
  await user.save();

  res.status(200).json({
    status: 'success',
  })
})