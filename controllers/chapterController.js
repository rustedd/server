const Chapter = require('../models/chapterModel');
const Particle = require('../models/particleModel');
const App = require('../models/appModel')
const catchAsync = require("../utils/catchAsync");
const APIFeatures = require('./../utils/apiFeatures');
const mongoose = require('mongoose')

const AppError = require('../utils/appError')

exports.getChapters = catchAsync(async (req, res, next) => {
  
  // let chapters = await Chapter.find();
  
  let filter = { isDeleted: { "$ne": true } };
  if (req.params.appId) {
    filter.appId = req.params.appId;
  }

  const chapters = new APIFeatures(Chapter.find(filter), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate();
  // const doc = await features.query.explain();
  const doc = await chapters.query;

  res.send(doc)

});

exports.getChapterDetails = catchAsync(async (req, res, next) => {
  let chapter = await Chapter
    .findById(req.params.id)

  let chapterParticles = await Particle.find({ chapter: mongoose.Types.ObjectId(req.params.id), isDeleted: { "$ne": true } }).sort({ rank: 1 })

  if (!chapter) {
    return next(new AppError(`No chapter found with that id`, 404))
  }

  let { isPublished, isPremium, _id, title, description, app, segment, rank, createdAt, updatedAt } = chapter;
  let data = {
    particles : chapterParticles, 
    isPublished, 
    isPremium, 
    _id, 
    title, 
    description, 
    app, 
    segment, 
    rank, 
    createdAt, 
    updatedAt
  }

  res.send(data)
})

exports.createChapter = catchAsync(async (req, res, next) => {
  if(!req.body.app) return res.status(500).json({ error: "appId is missing"})

  let newChapter = await Chapter.create(req.body);

  res.status(200).json({
    newChapter: newChapter,
    status: 'success'
  })
})

exports.deleteChapter = catchAsync(async (req, res, next) => {
  let chapter = await Chapter.findByIdAndUpdate(req.params.id, { isDeleted: true })
  await Particle.updateMany({ chapter }, { isDeleted: true })

  res.status(204).json({
    status: 'success',
    data: chapter
  });
})

exports.patchChapter = catchAsync(async (req, res, next) => {
  let chapter = await Chapter.findOneAndUpdate(
    { _id: req.params.id }, 
    req.body, 
    { new: true }
  );

  if(!chapter){
    return res.status(500).send({ error: "Chapter does not exist"});
  }

  res.send({ chapter })
});

exports.getChapterWithStatus = catchAsync(async (req, res, next) => {

})