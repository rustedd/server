const { promisify } = require('util');
const jwt = require('jsonwebtoken')
const User = require('../models/userModel');
const App = require('../models/appModel');
const Particle = require('../models/particleModel');
const Chapter = require('../models/chapterModel')
const Segment = require('../models/segmentModel')
const AppError = require('../utils/appError')
const catchAsync = require("../utils/catchAsync");

const signToken = id => {
  return jwt.sign({ id: id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  }); 
}

exports.signup = catchAsync(async (req, res, next) => {
  const newUser = await User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm
  });

  const token = signToken(newUser._id)

  res.status(201).json({
    status: 'success',
    token: token,
    data: {
      user: newUser
    }
  })
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if(!email || !password){
    return next(new AppError('Please provide email and password ', 400));
  }

  const user = await User.findOne({ email }).select('+password')

  if(!user || !await user.correctPassword(password, user.password)){
    return next(new AppError('Incorrect email or password ', 401))
  }

  const token = signToken(user._id);
  res.status(200).json({
    status: 'success',
    token,
    email
  })
})

exports.protect = catchAsync(async (req, res, next) => {
  let token;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  }

  if(!token) {
    return next(new AppError('You are not logged in ', 401));
  }
  
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)
  
  const freshUser = await User.findById(decoded.id);
  if(!freshUser) {
    return next(new AppError('the user belonging to the id no longer exists', 401));
  }
  
  if(freshUser.changedPasswordAfter(decoded.iat)){
    return next(new AppError('User changed password log in again', 401))
  }

  req.user = freshUser; 

  next()
});

exports.checkAppEditPermission = catchAsync(async (req, res, next) => {
  let appId = req.body.appId || req.params.id;
  const app = await App.findById(appId);

  if(!app){
    return next(new AppError('Invalid app id', 400));
  };

  if(!app.owner.equals(req.user._id)){
    return next(new AppError('Not authorized', 401))
  }

  req.appData = app;

  next();
});


exports.checkParticleEditPermission = catchAsync(async (req, res, next) => {
  const particle = await Particle.findById(req.params.id);
  const app = await App.findById(particle.appId);

  if(!app.owner.equals(req.user._id)){
    return next(new AppError('Not authorized', 401))
  };

  req.appData = app;

  next();
})

exports.checkChapterEditPermission = catchAsync(async (req, res, next) => {
  const chapter = await Chapter.findById(req.params.id);
  const app = await App.findById(chapter.app);

  if(!app.owner.equals(req.user._id)){
    return next(new AppError('Not authorized', 401))
  };

  req.appData = app;

  next();
})

exports.checkSegmentEditPermission = catchAsync(async (req, res, next) => {
  const segment = await Segment.findById(req.params.id);
  const app = await App.findById(segment.app);

  if(!app.owner.equals(req.user._id)){
    return next(new AppError('Not authorized', 401))
  };

  req.appData = app;

  next();
})