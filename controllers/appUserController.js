const AppUser = require('../models/appUserModel');

const AppError = require('../utils/appError');
const catchAsync = require("../utils/catchAsync");
const user = require('../models/appUserModel');

exports.updateUser = catchAsync(async (req, res, next) => {
  console.log(req.user);
  const user = await AppUser.findById(req.user._id);
  let { onesignalId, username, name } = req.body;

  if(username) {
    user.username = username;
    if(!(/^[a-zA-Z0-9]+$/.test(username))){
      return res.status(500).json({
        status: 'failure',
        message: 'username can contain only letters and numbers'
      })
    }
  }

  if(name) {
    user.name = name;
  }

  if(onesignalId) {
    user.onesignalId = onesignalId;
  }

  console.log("new user ", user);
  await user.save();

  res.status(200).json({
    status: 'success',
    name,
    username,
    onesignalId
  })
});

exports.checkUsername = catchAsync(async (req, res, next) => {
  let { appId, username } = req.query;
  const user = await AppUser.findOne({
    appId,
    username
  });

  if(user){
    res.status(200).json({
      status: 'success',
      userIdAvailable: false
    })
  } else {
    res.status(200).json({
      status: 'success',
      userIdAvailable: true
    })
  }
})