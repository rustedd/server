const express = require('express')
const app = express()
const cors = require('cors')
const morgan = require('morgan');

const chapterRouter = require('./routes/chapterRoutes');
const particleRouter = require('./routes/particleRoutes');
const userRouter = require('./routes/userRoutes')
const appRouter = require('./routes/appRoutes');
const sessionRouter = require('./routes/sessionRoutes');
const segmentRouter = require('./routes/segmentRoutes');
const appUserRouter = require('./routes/appUserRoutes');
const studentRouter = require('./routes/studentRoutes');
const globalErrorHandler = require('./controllers/errorController')

const AppError = require('./utils/appError')

app.use(cors());
app.use(morgan('combined'))
app.use(express.json({ limit: '2MB' }));
app.use(express.urlencoded({ extended: true, limit: '2MB' }));

app.use('/user', userRouter);
app.use('/app', appRouter);
app.use('/chapter', chapterRouter);
app.use('/particle', particleRouter);
app.use('/session', sessionRouter);
app.use('/appuser', appUserRouter);
app.use('/segment', segmentRouter);
app.use('/student', studentRouter);

app.get('/hello', (req, res) => res.status(200).json({'Hello': "world"}))
app.post('/hello', (req, res) => res.status(200).json({'Hello': "world"}))

app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
})

app.use(globalErrorHandler)

module.exports = app;