const mongoose = require("mongoose");

const segmentSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: String,
  isPublished: {
    type: Boolean,
    default: false
  }, // to unpublish a segmenticular lesson
  app: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App'
  },
  rank: {
    type: Number,
    required: true
  },
  type: {
    type: String, // this is to store the type of segment (if it is a game or not)
    enum: ["GAME"]
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
}, { timestamps: true });

const segment = mongoose.model('Segment', segmentSchema);

module.exports = segment;