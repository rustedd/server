const mongoose = require('mongoose');

const sessionSchema = new mongoose.Schema({
  user: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AppUser'
  },
  chapter: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Chapter'
  },
  app: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App'
  },
  correctAnswers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Particle'
  }],
  wrongAnswers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Particle'
  }],
  status: {
    type: String,
    enum: ["INCOMPLETE", "COMPLETE"]
  }
})

const session = mongoose.model('Session', sessionSchema);

module.exports = session;