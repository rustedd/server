const mongoose = require("mongoose");

const appSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: String,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  isGenericApp: Boolean,
  isDeleted: {
    type: Boolean,
    default: false
  }
}, { timestamps: true });

const app = mongoose.model('App', appSchema);

module.exports = app;