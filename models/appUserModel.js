const mongoose = require("mongoose");
const validator = require('validator');

const bcrypt = require('bcryptjs');

const appUserSchema = new mongoose.Schema({
  email: {
    type: String,
    lowercase: true
  },
  username: {
    type: String,
    lowercase: true
  },
  appId: {
    type: String
  },
  password: {
    type: String,
    minlength: 8,
    select: false
  },
  passwordConfirm: {
    type: String,
    validate: {
      // This only works on CREATE and SAVE!!!
      validator: function(el) {
        return el === this.password;
      },
      message: 'Passwords are not the same!'
    }
  },
  name: String,
  onesignalId: String
}, { timestamps: true });

appUserSchema.index({ email: 1, appId: 1}, { partialFilterExpression: { email: { $exists: true } }, unique: true});
appUserSchema.index({ username: 1, appId: 1}, { partialFilterExpression: { username: { $exists: true } }, unique: true});

appUserSchema.pre('save', async function(next) {
  if(!this.isModified('password')) return next();
  this.password = await bcrypt.hash(this.password, 12)

  this.passwordConfirm = undefined;
  next();
});

appUserSchema.methods.correctPassword = async function(candidatePassword, userPassword) {
  return await bcrypt.compare(candidatePassword, userPassword)
}

appUserSchema.methods.changedPasswordAfter = function(JWTTimestamp) {
  if(this.passwordChangedAt) {
    const changedTimestamp = parseInt(this.passwordChangedAt.getTime()/1000, 10);
    return JWTTimestamp < changedTimestamp;
  }
  return false;
}


const user = mongoose.model('AppUser', appUserSchema);

module.exports = user;