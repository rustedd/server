const mongoose = require('mongoose');

const particleSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["NOTE", "MCQ-LEARN", "MCQ-QUIZ", "MCQ", "MEANING", "CONVERSATION", "JUMBLEDWORDS", "STACKEDNOTES"]
  },
  title: {
    type: String,
    trim: true
  },
  question: {
    type: String,
    trim: true
  },
  answer: {
    type: String,
    trim: true
  }, // this is for meaning and translate, question and answer
  description: {
    type: String,
    trim: true
  },
  text: {
    type: String,
    trim: true
  }, // this is in case of a note only
  stackedNotes: [{
    type: {
      type: String,
      enum: ["TEXT"]
    },
    heading: {
      type: String
    }, 
    text: {
      type: String
    },
  }],
  tip: {
    type: String,
    trim: true
  },
  hint: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Particle'
  },
  options: [{
    text: {
      type: String,
      trim: true
    },
    nextParticle: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Particle'
    },
    isAnswer: Boolean
  }],
  nextParticle: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Particle'
  },
  conversation: [{
    name: {
      type: String,
      trim: true
    },
    sentence: {
      type: String,
      trim: true
    },
    translation: {
      type: String,
      trim: true
    }
  }],
  appId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App'
  },
  rank: mongoose.Schema.Types.Number,
  chapter: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Chapter'
  },
  isDeleted: {
    type: mongoose.Schema.Types.Boolean,
    default: false
  }
}, { timestamps: true })

const particle = mongoose.model('Particle', particleSchema);

module.exports = particle;