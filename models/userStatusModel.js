const mongoose = require('mongoose');

const userStatusSchema = new mongoose.Schema({
  user: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  app: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App'
  },
  gameRankings:  {
    game: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Chapter'
    },
    score: {
      type: Number,
    }
  }
});

const userStatus = mongoose.model('UserStatus', userStatusSchema);

module.exports = userStatus;
