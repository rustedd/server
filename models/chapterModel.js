const mongoose = require("mongoose");

const ParticleModel = require('./particleModel')

const chapterSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  particles:[{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Particle'
  }],
  type: {
    type: String, 
    enum: ["GAME", "LESSON"] // right now without type is being taken as lesson
  },
  subType: {
    type: String, // this is used to store the type of game and also if it is quiz or learn
    enum: ["BTC"] //(BTC -> BEAT THE CLOCK)
  },
  goBack: Boolean, // if you can go back to the previous particle
  isPublished: {
    type: Boolean,
    default: false
  }, // to unpublish a particular lesson
  app: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App',
    required: true
  },
  segment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Segment',
    required: true
  },
  rank: {
    type: Number,
    required: true
  },
  isPremium: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: mongoose.Schema.Types.Boolean,
    default: false
  }
}, { timestamps: true });

chapterSchema.pre('remove', async function(next) {
  try {
    let t = await ParticleModel.remove({ _id: {'$in': this.particles}}).exec()
  } catch (err){
    console.log(err)
  }
  next();
})


const chapter = mongoose.model('Chapter', chapterSchema);

module.exports = chapter;