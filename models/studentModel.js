const mongoose = require("mongoose");

const studentSchema = new mongoose.Schema({
  studentEmail: {
    type: String
  },
  appId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'App'
  }
})

studentSchema.index({ studentEmail: 1, appId: 1 }, { unique: true })

const student = mongoose.model('Student', studentSchema);

module.exports = student;