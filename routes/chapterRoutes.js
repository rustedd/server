const express = require('express');
const chapterController = require('../controllers/chapterController');
const authController = require('../controllers/authController');

const router = express.Router();

router
  .route('/')
  .get(chapterController.getChapters)
  .post(chapterController.createChapter)

router
  .route('/:id')
  .get(chapterController.getChapterDetails)
  .patch([authController.protect, authController.checkChapterEditPermission], chapterController.patchChapter)
  .delete([authController.protect, authController.checkChapterEditPermission], chapterController.deleteChapter)

module.exports = router;