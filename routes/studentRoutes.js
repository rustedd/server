const express = require('express');
const studentController = require("../controllers/studentController");
const authController = require('../controllers/authController');

const router = express.Router();

router
  .route('/')
  .get(studentController.getStudent)
  .post([authController.protect, authController.checkAppEditPermission], studentController.postStudent)
  .delete([authController.protect], studentController.deleteStudent)
router
  .route('/all-students')
  .get(studentController.getAllStudents)

module.exports = router;