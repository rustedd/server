const express = require('express');
const particleController = require('../controllers/particleController');

const router = express.Router();

router
  .route('/')
  .post(particleController.addParticle)
  // .patch(particleController.patchParticle)

router 
  .route('/:id')
  .delete(particleController.deleteParticle)
  .patch(particleController.patchParticle)

router
  .route('/import-from-google')
  .post(particleController.importFromGoogle)
module.exports = router;