const express = require('express');
const appController = require('../controllers/appController');
const authController = require('../controllers/authController');
const appUserAuthController = require('../controllers/appUserAuthController');

const router = express.Router();

router
  .route('/')
  .get(authController.protect, appController.getApps) // get list of apps [DASHBOARD ROUTE]
  .post(authController.protect, appController.createApp) // post new app [DASHBOARD ROUTE]

router
  .route('/:id')
  .get(appController.getAppDetails) // fetches all the chapters under the app [DASHBOARD ROUTE]
  .delete([authController.protect, authController.checkAppEditPermission], appController.deleteApp) //[DASHBOARD ROUTE]
  .patch([authController.protect, authController.checkAppEditPermission], appController.patchParticle) //[DASHBOARD ROUTE]


/*
 * pass query param `type` as GAME to get only games.
 * passing nothing will send everything other than games
 */


router
  .route('/with-status/:id')
  .get([appUserAuthController.protect],appController.getAppDetailsWithStatus) // [APP ROUTE]
//TODO: DEPRECATED, delete after testing
// router
//   .route('/update-app/:id')
//   .get(appController.updateChapterList)
  // router
//   .route('/:id')
//   .get(appController.getChapterDetails)

module.exports = router;