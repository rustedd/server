const express = require('express');
const sessionController = require('../controllers/sessionController');
const appUserAuthController = require('../controllers/appUserAuthController');

const router = express.Router();

router
  .route('/')
  .get(sessionController.getSessions)
  .post(appUserAuthController.protect, sessionController.postSessions)

// router
//   .route('/:id')
//   .get(chapterController.getChapterDetails)
//   .patch([authController.protect, authController.checkChapterEditPermission], chapterController.patchChapter)

module.exports = router;