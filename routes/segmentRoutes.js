const express = require('express');
const segmentController = require('../controllers/segmentController');
const authController = require('../controllers/authController');

const router = express.Router();

router
  .route('/')
  .get(authController.protect, segmentController.getSegments) // get list of apps
  .post([authController.protect, authController.checkAppEditPermission], segmentController.createSegment) // post new app

router
  .route('/:id')
  // .get(appController.getAppDetails)
  .delete([authController.protect, authController.checkSegmentEditPermission], segmentController.deleteSegment)
  .patch([authController.protect, authController.checkSegmentEditPermission], segmentController.patchSegment)

module.exports = router;