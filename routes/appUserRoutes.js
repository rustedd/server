const express = require('express');
const appUserAuthController = require('../controllers/appUserAuthController');
const appUserController = require("../controllers/appUserController");

const router = express.Router();

router.post('/signup', appUserAuthController.signup);
router.post('/login', appUserAuthController.login);

/*
* used to update the email and password for anonymous users 
*/
router.patch('/updateme', 
  appUserAuthController.protect, 
  appUserAuthController.updateUser
); 


/*
 * Updates onesignal token 
 * SHOUlD BE DEPRECATED!!!!
 */
router.patch('/update-token', 
  appUserAuthController.protect, 
  appUserAuthController.updateToken
); 

/*
 * updates everything that doesn't have anything to do  with authorization
 * username, name, onesignalId
 */

router.patch('/update-user',
  appUserAuthController.protect,
  appUserController.updateUser
)

router.get(`/check-username`,
  appUserController.checkUsername
);


module.exports = router;